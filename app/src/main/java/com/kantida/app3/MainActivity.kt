package com.kantida.app3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kantida.app3.model.Oil

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val oilList: List<Oil> = listOf(
            Oil(37.24, "เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20"),
            Oil(38.08, "เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91"),
            Oil(38.35, "เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95"),
            Oil(45.84, "เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95"),
            Oil(36.34, "เชลล์ ดีเซล B20"),
            Oil(36.34, "เชลล์ ฟิวเซฟ ดีเซล"),
            Oil(36.34, "เชลล์ ฟิวเซฟ ดีเซล B7"),
            Oil(36.34, "เชลล์ วี-เพาเวอร์ ดีเซล"),
            Oil(47.06, "เชลล์ วี-เพาเวอร์ ดีเซล B7")
        )
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ItemAdapter(oilList)

    }
    class ItemAdapter(val oilList: List<Oil>) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
        class ViewHolder(private val itemView: View) : RecyclerView.ViewHolder(itemView) {
            val price = itemView.findViewById<TextView>(R.id.item_price)
            val name = itemView.findViewById<TextView>(R.id.item_name)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//
            holder.price.text = oilList[position].price.toString()
            holder.name.text = oilList[position].name
//
//            holder.itemView.setOnClickListener(View.OnClickListener {
//                Toast.makeText(
//                    holder.itemView.getContext(),
//                    "ชื่อน้ำมัน : " + holder.name.text + " ราคา : " + holder.price.text + " บาท/ลิตร",
//                    Toast.LENGTH_LONG
//                ).show()
//            })
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdapter.ViewHolder {
            val adapterLayout =
                LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
            return ViewHolder(adapterLayout)
        }

        override fun getItemCount(): Int {
            return oilList.size
        }

    }
}



